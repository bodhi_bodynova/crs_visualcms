<?php
namespace Ewald\VisualCMSWidget\Core;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\FileCache;
use OxidEsales\Eshop\Core\ConfigFile;


class Events{




    public static function getAWidget(){

        return "widgee.php";

    }

    public static function onActivate(){
        $sModulesDir = dirname(__FILE__,5).'/modules/ewald/crs_visualCMS/Core/';


        // Ziel:
        $sModulesDir_Ziel = dirname(__FILE__,5).'/modules/ddoe/visualcms/Core/shortcodes/';

        // Copy:
        copy ( $sModulesDir.self::getAWidget() ,$sModulesDir_Ziel.self::getAWidget() );
        self::clearCache();
    }

    public static function onDeactivate(){

        unlink(dirname(__FILE__,5).'/modules/ddoe/visualcms/Core/shortcodes/'.self::getAWidget());
        self::clearCache();
    }

        /*
         Löscht den TMP-Ordner sowie den Smarty-Ordner
        */

    protected static function clearCache(){
        /* @var FileCache $fileCache */
        $fileCache = oxNew( FileCache::class );
        $fileCache::clearCache();

        /** Smarty leeren */
        $tempDirectory = Registry::get( ConfigFile::class )->getVar("sCompileDir");
        $mask = $tempDirectory . "/smarty/*.php";
        $files = glob($mask);
        if (is_array($files)) {
            foreach ($files as $file) {
                if (is_file($file)) {
                    @unlink($file);
                }
            }
        }
    }
}
