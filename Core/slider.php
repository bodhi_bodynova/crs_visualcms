<?php

/**
 * This Software is the property of OXID eSales and is protected
 * by copyright law.
 *
 * Any unauthorized use of this software will be prosecuted by
 * civil and criminal law.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2017
 * @version   OXID eSales Visual CMS
 */

use OxidEsales\VisualCmsModule\Application\Model\VisualEditorShortcode;
use OxidEsales\VisualCmsModule\Application\Model\Media;

use OxidEsales\Eshop\Core\Registry;

class slider_shortcode extends VisualEditorShortcode
{

    protected $_sTitle = 'Bannerslider';

    protected $_sBackgroundColor = 'red';

    protected $_sIcon = 'fa-circle-o';

    public function install()
    {
        $this->setShortCode(basename(__FILE__, '.php'));

        $oLang = Registry::getLang();

        $this->setOptions(
            array(
                'images' => array(
                    'type' => 'image',
                    'label' => $oLang->translateString('DD_VISUAL_EDITOR_WIDGET_IMAGES'),
                    'multi' => true,
                    'preview' => true,
                    'help' => 'Titel und Links von oben nach unten entsprechen den Bildern von links nach rechts.'
                ),
                'title' => array(
                    'type' => 'title',
                    'label' => 'Titel'
                ),'href' => array(
                'type' => 'href',
                'label' => 'Links',
                'help' => '(zB.&nbsp;https://bodynova.de/Seite)'
            ),'intern' => array(
                'type' => 'hidden'
            )
            )
        );

    }

    public function parse($sContent = '', $aParams = array())
    {

        $aTitel = explode(",",$aParams['title']);
        $aHref = explode(",",$aParams['href']);

        if( !$aParams[ 'images' ] || !is_array( $aParams[ 'images' ] ) )
        {
            return '';
        }

        $sInnerHTML = "";
        $oConfig = \OxidEsales\Eshop\Core\Registry::getConfig();
        $oMedia  = oxNew( Media::class );
        $count = 0;
        $countLinks = 0;

        foreach( $aParams[ 'images' ] as $i => $sFile ){
            $sURL  = $oMedia->getMediaUrl( $sFile );

            $title = $aTitel[$count];
            $href = $aHref[$countLinks];
            $countLinks++;
            $internextern = $aHref[$countLinks];

            if( $oConfig->isSsl() )
            {
                $sURL = str_replace( $oConfig->getShopUrl(), $oConfig->getSslShopUrl(), $sURL );
            }

            if($internextern == 'intern' && $href != ''){
                $href= $oConfig->getSslShopUrl() . $href;
            }

            $ref = $href == '' ? "" : 'href="' . $href . '"';
            $intern = $internextern == 'intern' ? "" : 'target="_blank"';


            $sInnerHTML .= ' 
                            <div class="owl-item" style="width: 1281px;">
                                <a '. $intern .' '.  $ref.' title="'.$title.'">
                                    <div class="owl-banner-box" style="background-image: url('.$sURL.'); background-repeat:no-repeat;background-position:center;background-size:cover"></div>
                                </a>
                            </div>';
            $count++;
            $countLinks++;
        }

        $sHTML = '
                <div id="owl-cms" class="owl-carousel owl-theme owl-loaded owlgap">																																	
                    <div class="owl-stage-outer">
                        <div class="owl-stage" >
                            '.$sInnerHTML.'
                        </div>
                    </div>
                  </div>                                                     
                  ';
        return $sHTML;
    }

}