<?php
$conf = \OxidEsales\Eshop\Core\Registry::getConfig();
$sModulesDir = $conf->getShopMainUrl().'modules/ewald/crs_visualCMS/';
/**
 * Metadata version
 */
$sMetadataVersion = '2.0';

$aModule = [
    'id'          => 'crs_visualCMS',
    'title'       => [
        'en' => '<img src="'.$sModulesDir.'out/img/favicon.ico" title="Visual CMS">Visual CMS Widget Module',
        'de' => '<img src="'.$sModulesDir.'out/img/favicon.ico" title="Visual CMS">Visual CMS Widget Modul',
    ],
    'description' => [
        'en' => 'Module for created widgets',
        'de' => 'Modul für neue Widgets',
    ],
    'thumbnail'   => 'out/img/logo_bodynova.png',
    'version'     => '2.0',
    'author'      => 'Bodynova GmbH',
    'url'         => 'https://bodynova.de',
    'email'       => 'c.ewald@bodynova.de',
    'controllers'       => [
        /*
        'dre_admindashboard'                        =>
            Bender\dre_AdminBoard\Extensions\Application\Controller\Admin\dre_admindashboard::class,

        'dre_admincontroller'   =>
        //\Bender\dre_AdminBoard\Application\Controller\Admin\dre_admincontroller::class,
            \Bender\dre_AdminBoard\Application\Controller\Admin\dre_admincontroller::class,
        */
    ],
    'extend'      => [
        /*
        \OxidEsales\Eshop\Application\Controller\Admin\AdminStart::class            =>
            \Bender\dre_AdminBoard\Extensions\Application\Controller\Admin\dre_adminboardstart::class,
        \OxidEsales\Eshop\Application\Controller\Admin\NavigationController::class  =>
            \Bender\dre_AdminBoard\Extensions\Application\Controller\Admin\dre_admindashboard::class
        */
    ],
    'templates'   => [
        /*
        'dre_start.tpl'                   => 'bender/dre_adminboard/Application/views/admin/tpl/dre_start.tpl',
        'dre_adminboard.tpl'              => 'bender/dre_adminboard/Application/views/admin/tpl/dre_adminboard.tpl',
        'dre_orderchart.tpl'              => 'bender/dre_adminboard/Application/views/admin/tpl/dre_orderchart.tpl',
        'dre_startmenu.tpl'               => 'bender/dre_adminboard/Application/views/admin/tpl/dre_startmenu.tpl',
        */
    ],
    'blocks'      => [],
    'settings'    => [
        /*
        [
            'group' => 'dre_AdminBoard',
            'name'  => 'dre_AdminBoard_orders',
            'type'  => 'bool',
            'value' => '0'
        ] */
    ],
    'events' => [
        'onActivate' =>
            'Ewald\VisualCMSWidget\Core\Events::onActivate',
        'onDeactivate' =>
            'Ewald\VisualCMSWidget\Core\Events::onDeactivate',
    ],
];